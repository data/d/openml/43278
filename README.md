# OpenML dataset: Graph_Inference_Dataset

https://www.openml.org/d/43278

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

//Add the description.md of the data file Graph_Inference_Dataset

Goudet, Olivier, 2017, "Graph inference datasets. Replication Data for: "Learning Functional Causal Models with Generative Neural Networks"",

[Link](https://doi.org/10.7910/DVN/UZMB69), Harvard Dataverse, V1, UNF:6:wrgpGhxTNPqE4R5S2cNcpg== [fileUNF] 


Graph datasets in csv format. Used in the article Learning Functional Causal Models with Generative Neural Networks.
1) File *_numdata.csv contain the data of around 20 variables connected in a graph without hidden variables. G2, G3, G4 and G5 refered to graph with 2, 3, 4 and 5 parents maximum for each node. Each file *_target.csv contains the ground truth of the graph with cause -> effect File beginning by "Big" are larger graphs with 100 variables.
2) Each file *_confounders_numdata.csv contain the data of around 20 variables connected in a graph. There are 3 hidden variables. Each file *_confounders_skeleton.csv contains the skeleton of the graph (including spurious links due to common hidden cause). Each file *_confounders_target.csv contains the ground truth of the graph with the direct visible cause -> effect. The task is to recover the direct visible links cause->effect while removing the spurious links of the skeleton
(2017-08-24)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43278) of an [OpenML dataset](https://www.openml.org/d/43278). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43278/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43278/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43278/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

